<?php


use System\Router;
use App\Config;

/**
 * returns an URL address by name
 *
 * @param $name
 * @param array $params
 * @return mixed|string
 */
function route($name, $params = [])
{
    if (isset(Router::$route_name[$name])) {
        $url = Config::DOMAIN . Router::$route_name[$name];
        if (!empty($params)){
            foreach ($params as $key => $param){
                $url = str_replace('{'.$key.'}', $param,$url);
            }
        }
        return $url;
    } else {
        echo 'Route name does not exist';
        die;
    }
}

/**
 * redirects to $url
 *
 * @param string $url
 */
function redirect($url = '/')
{
    header("location: $url");
}


/**
 * prints $data
 *
 * @param $data
 */
function dd($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die;
}

/**
 * converts the address from the string to the URL
 *
 * @param $str
 * @param array $replace
 * @param string $delimiter
 * @return mixed|string
 */
function slugit($str, $replace=array(), $delimiter='-') {
    if ( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    return $clean;
}

/**
 *
 *
 * @param string $path
 * @return string
 */
function asset($path = ''){
    return Config::DOMAIN.$path;
}