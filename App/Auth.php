<?php

namespace App;


use App\Models\User;


class Auth
{

    public static function rememberLogin($token)
    {
        $model = new User();
        $user = $model->rememberUser($token);
        return $user;
    }

}
