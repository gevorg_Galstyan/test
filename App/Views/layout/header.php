<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= \App\Config::APP_NAME ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

</head>
<body style="height:1500px">
<?php
$langs = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$langs = $_COOKIE['lang'];
$language = New \App\Controllers\LanguageController([],$langs);
$langArray = array();
$langArray =  $language->userLanguage();
?>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="<?= route('home') ?>">Logo</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?= route('home') ?>"><?= $langArray['home'] ?></a>
        </li>
    </ul>

    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <?php if (!isset($_SESSION['user']) || empty($_SESSION['user'])): ?>

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $langArray['select-language'] ?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?php foreach (\App\Config::LANG as $key => $value) { ?>
                        <a class="dropdown-item" href="<?= route('change_lang',['lang' => $key])?>"><? echo $value?></a>
                        <?php } ?>

                    </div>
                </div>
                <li class="nav-item">
                    <a class="nav-link"  href="<?= route('home') ?>"> <?= $langArray['login'] ?> </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= route('get-register') ?>"><?= $langArray['register'] ?></a>
                </li>

            <?php else: ?>

                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $langArray['select-language'] ?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <?php foreach (\App\Config::LANG as $key => $value) { ?>
                            <a class="dropdown-item" href="<?= route('change_lang',['lang' => $key])?>"><? echo $value?></a>
                        <?php } ?>

                    </div>
                </div>

                <li class="nav-item">
                    <a class="nav-link" href="<?= route('logout') ?>"><?= $langArray['log-out'] ?></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>
<?php if (isset($_SESSION['message'])): ?>
    <?php if (is_array($_SESSION['message'])): ?>
        <?php foreach ($_SESSION['message'] as $message): ?>
            <div class="alert alert-<?= $message['alert'] ?> alert-dismissible fade show mt-5 pt-4" role="alert">
                <strong><?= $message['text'] ?></strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="alert alert-info alert-dismissible fade show mt-5 pt-4" role="alert">
            <strong><?= $_SESSION['message'] ?></strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (isset($_SESSION['message'])) {
        unset($_SESSION['message']);
    } ?>
<?php endif; ?>
