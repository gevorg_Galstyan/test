<?php if (!isset($_SESSION['user']) || empty($_SESSION['user'])): ?>

<?php endif; ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.js"></script>

<scrpt>
    <?php if (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'ru'):?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/localization/messages_ru.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/localization/messages_ru.min.js"></script>
    <?php endif; ?>
</scrpt>
<script>
    <?php if (isset($_COOKIE['lang'])):?>
        var local = '<?= $_COOKIE['lang']?>';
    <?php else:?>
        var local = 'en';
    <?php endif;?>
</script>
<script src="<?= asset('js/custom.js')?>"></script>

</body>
</html>