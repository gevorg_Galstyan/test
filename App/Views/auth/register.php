<div class="container">


    <?php if (isset($_SESSION['error']['email'])): ?>
        <?php foreach ($_SESSION['error']['email'] as $item): ?>
            <strong class="error"> <?= $item ?> </strong>
        <?php endforeach; ?>
    <?php endif; ?>

    <form class="needs-validation mt-5 pt-3 ajax-form" id="register-form" action="<?= route('set-register') ?>" method="post"
          enctype="multipart/form-data">
        <div class="form-row">
            <div class="col-12 mb-3">
                <label for="name"><?= $langArray['name'] ?></label>
                <input type="text" class="form-control" id="name" placeholder="<?= $langArray['name'] ?>" name="name" required>


            </div>
            <div class="col-12 mb-3">
                <label for="email"><?= $langArray['email']?></label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="<?= $langArray['email']?>"
                           aria-describedby="inputGroupPrepend" required>

            </div>
            <div class="col-12 mb-3">
                <div class="input-group">
                    <label for="image-register"><?=$langArray['upload-image']?></label>
                    <input type="file" class="form-control-file" name="image" id="image-register">

                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="password"><?= $langArray['password'] ?></label>
                <input type="password" class="form-control" id="password" name="password" placeholder="<?= $langArray['password'] ?>"
                       required>

            </div>
            <div class="col-md-6 mb-3">
                <label for="confirm-password"><?= $langArray['confirm-password'] ?></label>
                <input type="password" class="form-control" id="confirm-password" name="confirm-password"
                       placeholder="<?= $langArray['confirm-password'] ?>" required>

            </div>
        </div>

        <input type="submit" name="submit" value="<?=$langArray['register'] ?>" class="btn btn-primary">
    </form>


</div>
