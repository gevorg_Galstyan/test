<div class="container" style="margin-top:80px">
    <div class="row">

        <?php if (!isset($_SESSION['user']) || empty($_SESSION['user'])): ?>

        <form class="needs-validation mt-5 pt-3 ajax-form" action="<?= route('login') ?>"
              method="post" novalidate>
            <div class="modal-body">

                <div class="form-row">
                    <div class="col-12 mb-3">
                        <label for="email"><?= $langArray['email'] ?></label>
                        <div class="input-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="<?= $langArray['email'] ?>"
                                   aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                                <?= $langArray['invalid-email-format']?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 mb-3">
                        <label for="password"><?= $langArray['password'] ?></label>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="<?= $langArray['password'] ?>" required>
                        <div class="invalid-feedback">
                           <?= $langArray['please-fill-the-field']?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-input" type="checkbox" name="remember" id="remember">
                        <label class="form-check-label" for="remember">
                            <?= $langArray['remember-me'] ?>
                        </label>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="submit"><?= $langArray['login'] ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= $langArray['close'] ?></button>
            </div>
        </form>
        <?else: ?>

        <h3><?= $langArray['you-are-logged-in']?></h3>
        <?endif; ?>


    </div>
</div>
