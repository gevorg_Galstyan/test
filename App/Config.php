<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config
{
    /**
     * site title
     * @var string
     */

    const APP_NAME = 'Forum';

    /**
     * site domain
     * @var string
     */
    const DOMAIN = 'http://test.loc/';

    /**
     * Database host
     * @var string
     */
    const DB_HOST = 'localhost';

    /**
     * Database name
     * @var string
     */
    const DB_NAME = 'forum';

    /**
     * Database user
     * @var string
     */
    const DB_USER = 'root';

    /**
     * Database password
     * @var string
     */
    const DB_PASSWORD = '';

    const LANG = ['ru'=>"Russian","en"=>"English"];

    const PUBLIC_PATH = __DIR__;
}
