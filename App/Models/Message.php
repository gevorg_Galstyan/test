<?php

namespace App\Models;


use System\Model;

class Message extends Model
{
    /**
     * $this table name
     */
    private $table = 'messages';


    /**
     * create row in messages table
     *
     * @param array $data
     * @return bool|\mysqli_result
     */
    public function create($data = [])
    {
        return $this->insert($this->table, $data);
    }

}