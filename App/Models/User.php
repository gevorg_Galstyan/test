<?php

namespace App\Models;

use System\Model;



class User extends Model
{
    /**
     * $this table name
     */

    private $table = 'users';


    /**
     * Create user
     *
     * @param array $data
     * @return bool|\mysqli_result
     */
    public function create($data = [])
    {
        if ($this->insert($this->table, $data)) {
            return $this->conn->insert_id;
        }
        return false;
    }

    /**
     * user verification during registration
     * @param $email
     * @return array|mixed
     */
    public function checkedUser($email)
    {
        $email = $this->escape($email);
        $sql = "select * from $this->table where `email` = '$email'";
        $data = $this->select($sql, 'first');
        return $data;
    }

    /**
     * updated user
     *
     * @param array $data
     * @param $where
     * @return bool|\mysqli_result
     */
    public function updateUser($data = [], $where)
    {
        return $this->update($this->table, $data, $where);
    }

    /**
     * select the user
     *
     * @param $data
     * @return array|bool|mixed
     */
    public function selectUser($data)
    {
        $email = $this->escape($data['email']);
        $password = $data['password'];
        $sql = "select * from $this->table where `email` = '$email' ";
        $user = $this->select($sql, 'first');
        if ($user) {
            if (password_verify($password, $user['password'])) {
                return $user;
            } else {
                return false;
            }
        }
    }

    /**
     * add remember in user
     *
     * @param $token
     * @return array|mixed
     */
    public function rememberUser($token)
    {
        $token = $this->escape($token);
        $sql = "select * from $this->table where `remember_token` = '$token' and `verify` = 1";
        return $this->select($sql, 'first');
    }

}
