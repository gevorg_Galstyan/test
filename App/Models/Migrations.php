<?php

namespace App\Models;


use System\Model;

class Migrations extends Model
{

    /**
     * created users table
     *
     * @return bool
     */
    public function createUserTable()
    {

        $sql = "CREATE TABLE users (
                id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(30) NOT NULL,
                email VARCHAR(50) UNIQUE,
                password VARCHAR(255) NOT NULL,
                remember_token VARCHAR(255) UNIQUE,
                verify_token VARCHAR(255) ,
                verify INT (1) NOT NULL DEFAULT 0,
                created_at TIMESTAMP
                )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

        if ($this->conn->query($sql) === TRUE) {
            $_SESSION['message'][] = ['alert' => 'success', 'text' => 'Table Users created successfully'];
            return true;
        } else {
            $_SESSION['message'][] = ['alert' => 'warning', 'text' => "Error creating table users: " . $this->conn->error];
            return false;
        }

    }

    /**
     * created themes table
     *
     * @return bool
     */
    public function createThemeTable()
    {

        $sql = "CREATE TABLE `themes` (
            id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            `user_id` INT(10) UNSIGNED  ,
            `slug` VARCHAR (255) UNIQUE  ,
            `name` VARCHAR (255),
            FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE
            )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

        if ($this->conn->query($sql) === TRUE) {
            $_SESSION['message'][] = ['alert' => 'success', 'text' => 'Table Themes created successfully'];
            return true;
        } else {
            $_SESSION['message'][] = ['alert' => 'warning', 'text' => "Error creating table users: " . $this->conn->error];
            return false;
        }

    }

    /**
     * created messages table
     *
     * @return bool
     */
    public function createMessageTable()
    {

        $sql = "CREATE TABLE `messages` (
            id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            `user_id` INT(10) UNSIGNED  ,
            `theme_id` INT(10) UNSIGNED   ,
            `text` VARCHAR (255),
            `created_at` TIMESTAMP,
            FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
            FOREIGN KEY(theme_id) REFERENCES themes(id) ON UPDATE CASCADE ON DELETE CASCADE
            )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";

        if ($this->conn->query($sql) === TRUE) {
            $_SESSION['message'][] = ['alert' => 'success', 'text' => 'Table Messages created successfully'];
            return true;
        } else {
            $_SESSION['message'][] = ['alert' => 'warning', 'text' => "Error creating table users: " . $this->conn->error];
            return false;
        }
    }


    /**
     *
     * checks if there is a table $table in the database
     *
     * @param $table
     * @return bool
     */
    public function tableExist($table)
    {
        $data = $this->conn->query("SHOW TABLES LIKE '$table'");
        if ($data->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }

}