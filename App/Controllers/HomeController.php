<?php

namespace App\Controllers;

use App\Models\Theme;
use \System\View;
use \System\Controller;


class HomeController extends Controller
{

    /**
     * Show the index page
     *
     * @return void
     */


    public function index()
    {
        if ($_COOKIE['lang']){
            $lang =  $_COOKIE['lang'];
        }
       
        if ($lang){
            setcookie('lang',$lang,time()+(86400 * 30),'/');
        }else{
            setcookie('lang','ru',time()+(86400 * 30),'/');
        }


        $changeLang = new ChangeLang();
        $langArray = $changeLang->change();

        View::render('home',compact('langArray'));
    }

}
