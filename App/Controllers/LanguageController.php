<?php
/**
 * Created by PhpStorm.
 * User: MaximumCode
 * Date: 02.11.2018
 * Time: 12:43
 */

namespace App\Controllers;


use App\Config;
use System\Controller;

class LanguageController extends  Controller
{

    private $UserLng;
    private $langSelected;
    public $langs = array();

    public function __construct(array $route_params,$userLanguage = 'ru')
    {
        parent::__construct($route_params);
        $this->UserLng = $userLanguage;
        $langFile = Config::PUBLIC_PATH.'\lang\\'. $this->UserLng . '.ini';
        $this->langs = parse_ini_file($langFile);

    }

    public  function changeLang()
    {


        $lang =  $this->route_params['lang'];
        if ($lang){
            setcookie('lang',$lang,time()+(86400 * 30),'/');
        }



        return redirect($_SERVER['HTTP_REFERER']);

    }

    public function userLanguage()
    {
        return $this->langs;
    }

}