<?php

namespace App\Controllers\Auth;


use App\Config;
use App\Controllers\ChangeLang;
use App\Controllers\LanguageController;
use App\Models\User;
use System\Controller;
use System\View;

class AuthController extends Controller
{

    /**
     * AuthController constructor.
     * @param array $route_params
     */
    public function __construct(array $route_params)
   {
       parent::__construct($route_params);

       if (isset($_SESSION['user']) && !empty($_SESSION['user']) && $route_params !='logout' ){
           return redirect(route('home'));
       }
   }

    /**
     * return register view
     */


    public function register()
    {

        $lang =  $_COOKIE['lang'];

        if ($lang){
            setcookie('lang',$lang,time()+(86400 * 30),'/');
        }
        $changeLang = new ChangeLang();
        $langArray =   $changeLang->change();
        $lang =  Config::LANG;



        return View::render('auth/register', compact('lang','langArray'));
    }



    /**
     * add user
     */
    public function set_register()
    {


        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            return redirect();
        } else {
            if (($_FILES['image']['name'])) {
                $target_dir = "uploads/";
                $imageFileType = strtolower(pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));
                $imageName = $_FILES["image"]["name"] = time() . rand(1, 999999999) . '.' . $imageFileType;
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $typeArr = ['jpg', 'png', 'gif'];
            }

            if (!isset($_COOKIE['lang']))
            {
                setcookie('lang','ru',time()+(86400 * 30),'/');
            }


            $data['name'] = $_POST['name'] ?? '';
            $data['email'] = $_POST['email'] ?? '';
            $data['password'] = $_POST['password'] ?? '';
            $data['image'] = $imageName ?? '';
            $confirm_password = $_POST['confirm-password'] ?? '';


            if (empty($data['name']) || empty($data['email']) || empty($data['password'])) {
                $messages = '';
                foreach ($data as $key => $item) {
                    if (empty($key)) {
                        $messages[] = 'Please fill the field';
                    }
                }
                echo json_encode(['error' => true, 'message' => $messages]);
                die;
            }

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                echo json_encode(['error' => true, 'message' => 'Invalid email format']);
                die;
            }
            if (strlen($data['password']) < 6) {
                echo json_encode(['error' => true, 'message' => 'Password must be at least 6 characters long']);
                die;
            }
            if ($data['password'] !== $confirm_password) {
                echo json_encode(['error' => true, 'message' => 'Password confirmation is not correct']);
                die;
            }
            if (($_FILES['image']['name'])){
            if (!in_array($imageFileType,$typeArr)) {

                echo json_decode(['error' => true, 'message' =>"Sorry, only JPG, PNG & GIF files are allowed." ]);
                die;
            }else{
                move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
            }
            }

            $data['remember_token'] = '';
            $data['created_at'] = date('Y-m-d H:i:s');
            $options = [
                'cost' => 12,
            ];
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT, $options);
            $model = New User();
            $user = $model->checkedUser($data['email']);
            if ($user) {
                    echo json_encode(['error' => true, 'message' => 'A user with this email already registered']);
                    die;
            }


            $data = $model->create($data);

                if ($data) {
                    $_SESSION['user'] = $data;
                    $_SESSION['message'][] = ['alert' => 'success', 'text' => 'We sent an email to your email address to confirm'];
                   redirect(route('home'));
                } else {
                    die;
                    echo json_encode(['error' => true, 'message' => 'You encountered a bug when you try to reload the page']);
                    die;
                }
            }
        }


    /**
     * login to the system
     */
    public function login(){
        if ($_SERVER["REQUEST_METHOD"] != "POST") {
            return redirect();
        }else{
            $data['email'] = $_POST['email'] ?? '';
            $data['password'] = $_POST['password'] ?? '';
            $remember = $_POST['remember'] ?? '';

            if (!isset($_COOKIE['lang']))
            {
                setcookie('lang','ru',time()+(86400 * 30),'/');
            }

            if (empty($data['email']) || empty($data['password'])) {
                $messages = '';
                foreach ($data as $key => $item) {
                    if (empty($key)) {
                        $messages[] = 'Please fill the field';
                    }
                }
                echo json_encode(['error' => true, 'message' => $messages]);
                die;
            }

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                echo json_encode(['error' => true, 'message' => 'Invalid email format']);
                die;
            }
            $model = new User();
            $user = $model->selectUser($data);
            if ($user || !empty($user)){
                if ($remember && $remember == 'on'){

                    $remember_token = md5(time() . $data['email'] . time());
                    $user_id = $user['id'];
                    $update = $model->updateUser(['remember_token' => $remember_token], "id = '$user_id'");
                    if ($update){
                        setcookie('remember', $remember_token, time()+(86400 * 30));
                    }

                }
                $_SESSION['user'] = $user;
                redirect(route('home'));
            }else{
                echo json_encode(['error' => true, 'message' => 'Email or password is incorrect']);
            }
        }
    }


    /**
     * Sign Out
     */
    public function logout(){
        unset($_SESSION['user']);
        setcookie('remember', '', time()-1);

        return redirect(route('home'));
    }

    /**
     * sending an email to the user after registration
     *
     * @param $email
     */

}