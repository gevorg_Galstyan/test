<?php
/**
 * Created by PhpStorm.
 * User: MaximumCode
 * Date: 02.11.2018
 * Time: 18:26
 */

namespace App\Controllers;


class ChangeLang
{

    /**
     * @return array|bool
     * this function returns array wich change language in site
     */

    public function change()
    {
        $langs = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $langs = $_COOKIE['lang'];
        $language = New LanguageController([],$langs);
        $langArray = array();
        $langArray =  $language->userLanguage();

        return $langArray;
    }
}