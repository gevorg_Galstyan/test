(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);

})();




$('.destroy-theme').click(function () {
    var url = $(this).data('delete');
    parent_div = $('div[data-status="'+$(this).data('target')+'"]');
    $.ajax({
        url: url,
        type: 'DELETE',
        success: function (i) {
            if (i == 1) {
                parent_div.fadeOut("slow", function() {
                    parent.remove();
                })
            }
        }
    })
});

$('#register-form').validate({
 lang:'german'
});



