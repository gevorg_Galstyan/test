<?php

session_start();

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';




/**
 * Routing
 */
$router = new System\Router();


$router->add('', ['controller' => 'HomeController', 'action' => 'index', 'name'=>'home']);


/**
 * AUTH Routing
 */
$router->add('register', ['controller' => 'AuthController', 'action' => 'register', 'namespace' => 'Auth', 'name' => 'get-register']);
$router->add('set-register', ['controller' => 'AuthController', 'action' => 'set_register', 'namespace' => 'Auth', 'name' => 'set-register']);
$router->add('login', ['controller' => 'AuthController', 'action' => 'login', 'namespace' => 'Auth', 'name' => 'login']);
$router->add('logout', ['controller' => 'AuthController', 'action' => 'logout', 'namespace' => 'Auth', 'name' => 'logout']);

/**
 * Theme Routing
 */

$router->add('theme/{slug}', ['controller' => 'HomeController', 'action' => 'singleTheme', 'name' => 'singleTheme']);
$router->add('create-theme', ['controller' => 'ThemeController', 'action' => 'create', 'name' => 'create-theme']);
$router->add('set-theme', ['controller' => 'ThemeController', 'action' => 'store', 'name' => 'set-theme']);
$router->add('send-comment/{slug}', ['controller' => 'ThemeController', 'action' => 'sendComment', 'name' => 'send-comment']);
$router->add('delete-theme/{slug}', ['controller' => 'ThemeController', 'action' => 'destroy', 'name' => 'destroy-theme']);


/**
 * DB Routing
 */
$router->add('db-migration', ['controller' => 'MigrationsController', 'action' => 'create', 'name'=>'db-migration']);
$router->add('db-seed', ['controller' => 'MigrationsController', 'action' => 'seed', 'name'=>'db-seed']);

/**
 * language Routing
 */
$router->add('change/{lang}', ['controller' => 'LanguageController','action' => 'changeLang','name'=>'change_lang']);

$router->dispatch($_SERVER['QUERY_STRING']);
