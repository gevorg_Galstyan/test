<?php

namespace System;



class View
{

    /**
     * Render a view file
     *
     * @param string $view The view file
     * @param array $args Associative array of data to display in the view (optional)
     *
     * @return void
     */
    public static function render($view, $args = [],$layout = true)
    {
        extract($args, EXTR_SKIP);

        $file = dirname(__DIR__) . "/App/Views/$view.php";

        if (is_readable($file)) {
            if ($layout){
                require  dirname(__DIR__) . '/App/Views/layout/header.php';
                require $file;
                require dirname(__DIR__) . '/App/Views/layout/footer.php';
            }else{
                require $file;
            }
        } else {
            echo "$file not found";
            die;
        }
    }
}
