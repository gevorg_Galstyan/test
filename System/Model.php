<?php

namespace System;


use App\Config;
use mysqli;



abstract class Model
{
    private $db = null;
    protected $conn = null;

    /**
     * Get the mysql database connection
     *
     * @return mixed
     */

    public function __construct()
    {
        $this->db = new mysqli(Config::DB_HOST, Config::DB_USER, Config::DB_PASSWORD);
        if ($this->db->connect_error) {
            $_SESSION['message'][] = ['alert' => 'warning','text' => "Connection failed: " . $this->db->connect_error];
            return false;
        }
        $data = $this->existDB(Config::DB_NAME);
        if (isset($data['error'])) {
            $_SESSION['message'][] = ['alert' => 'warning', 'text' => $data['message']];
            return false;
        } else {
            $this->conn = new mysqli(Config::DB_HOST, Config::DB_USER, Config::DB_PASSWORD, Config::DB_NAME);
        }
    }


    /**
     * check if the database is available, if not, then create
     *
     * @param $DBName
     * @return array|bool
     */
    private function existDB($DBName)
    {
        if (!$this->db->query("CREATE DATABASE IF NOT EXISTS $DBName CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;")) {
            $data = ['error' => true, 'message' => "Error creating database: " . $this->db->error];
            $this->db->close();
        } else {
            $data = true;
        }
        return $data;
    }


    /**
     * insert in table
     *
     * @param $table
     * @param $data
     * @return bool|\mysqli_result
     */
    protected function insert($table, $data){
        $name =[];
        $value = [];
        foreach($data as $key => $values) {
            $name[] = $key;
            $value[] = $this->escape($values);
        }
        $name = implode(',',$name);
        $value = implode("','",$value);

        $insert_query = "insert into $table ($name) values('$value')";
        return $this->conn->query($insert_query);

    }

    /**
     * select in table
     *
     * @param bool $query
     * @param string $count
     * @return array|mixed
     */
    protected function select($query = false, $count = 'get'){
        if ($query){
            $q = $this->conn->query($query);
        }
        $data = [];
        if ($q){
            if($count == 'first'){
                $data =  $q->fetch_assoc();
            }else{
                $data = $q->fetch_all(MYSQLI_ASSOC);

            }
        }

        return $data;
    }


    /**
     * update row in table
     *
     * @param $table
     * @param $data
     * @param $where
     * @return bool|\mysqli_result
     */
    protected function update($table, $data, $where){
        $name =[];
        foreach($data as $key => $values){
            $name[] = $key. "= '" . $this->escape($values)."'";
        }
        $name = implode(',',$name);
        $update = "update $table set $name where $where";
        if(!$update){
         $_SESSION['message'][] = ['alert' => 'warning', 'text' => $this->conn->connect_error];
         return false;
        }
        return $this->conn->query($update);
    }


    /**
     *
     *
     * @param $str
     * @return string
     */
    protected function escape($str){
        $str = $this->conn->escape_string($str);
        $str = htmlspecialchars($str);
        return $str;
    }


}
