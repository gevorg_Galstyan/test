<?php

namespace System;


use App\Auth;


abstract class Controller
{

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $route_params = [];

    protected $model;

    /**
     * Class constructor
     *
     * @param array $route_params Parameters from the route
     *
     * @return void
     */
    public function __construct($route_params)
    {
        $this->route_params = $route_params;
        require_once dirname(__DIR__) . '/App/helpers.php';
        if (isset($_COOKIE['remember']) && !isset($_SESSION['user'])){
            $user = Auth::rememberLogin($_COOKIE['remember']);
            if ($user){
                $_SESSION['user'] = $user;
            }
        }

    }

}
